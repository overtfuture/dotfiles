#!/bin/bash

INIT_DIR="$(dirname "$0")"

if [[ "$OSTYPE" == "linux-gnu" ]]; then

  PLUGIN_PATH=debian

  echo "--------------------------------"
  echo "Running Debian Scripts"
  echo "--------------------------------"
  echo ""

  sudo apt-get update &>/dev/null
  sudo apt-get upgrade -y &>/dev/null
elif [[ "$OSTYPE" == "darwin"* ]]; then

  PLUGIN_PATH=macos

  echo "--------------------------------"
  echo "Running macOS Scripts"
  echo "--------------------------------"
  echo ""
fi

if [ -f $INIT_DIR/$PLUGIN_PATH.env ]; then
  source $INIT_DIR/$PLUGIN_PATH.env
  export PROFILE_NAME
  export PROFILE_EMAIL
  export PROFILE_COMMENT
  export GPG_PASSPHRASE
  export SSH_PASSPHRASE
fi

for plugin in $(echo $PLUGINS | sed "s/,/ /g"); do
  echo "Running Plugin Script: $plugin"
  chmod +x $INIT_DIR/plugins/$PLUGIN_PATH/$plugin.sh
  sh "$INIT_DIR/plugins/$PLUGIN_PATH/$plugin.sh" -H || break
done

if hash brew 2>/dev/null; then
  brew update
  for package in $(echo $PACKAGES | sed "s/,/ /g"); do
    if !(brew ls --versions $package > /dev/null); then
      echo "Installing Brew Package: $package"
      brew install $package
    fi
  done
fi

if [[ "$OSTYPE" == "linux-gnu" ]]; then
  for package in $(echo $PACKAGES | sed "s/,/ /g"); do
    echo "Installing Package: $package"
    sudo apt-get install -y $package > /dev/null
  done
fi
