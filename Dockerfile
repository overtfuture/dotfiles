FROM ubuntu:eoan AS ubuntu-base

RUN apt-get update
RUN apt-get upgrade -y
RUN apt-get install sudo -y

FROM ubuntu-base

WORKDIR /developer/dotfiles

COPY . .

RUN ./init.sh

WORKDIR /developer/project

ENTRYPOINT ["zsh"]