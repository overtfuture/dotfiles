# Dot Files & Developer Environment

WIP: Use with caution, plugins and files will change without notice

## Installing

This setup focuses on using a single entrypoint, `init.sh`, to set up given plugins.

Plugins are found in `./plugins/PLATFORM`

Run this with ENV set for which plugins you want installed:

`PLUGINS=homebrew,git,zsh,neovim ./init.sh`

You can also just source a file with these ENV variables

```bash
source plugins.env
./init.sh
```

This is all set up to quickly launch a new shell environment anywhere
