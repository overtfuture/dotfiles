#!/bin/bash

if !(brew ls --versions fzf > /dev/null); then
  brew install fzf
fi

if [ ! -f $HOME/.fzf.zsh ]; then
    /usr/local/opt/fzf/install
fi