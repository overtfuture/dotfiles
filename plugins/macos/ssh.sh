#!/bin/bash
if [ ! -d "$HOME/.ssh" ]; then
    ssh-keygen -q -t ed25519 -N "$SSH_PASSPHRASE" -f ~/.ssh/id_rsa
fi
