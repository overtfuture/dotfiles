#!/bin/bash

if !(brew ls --versions neovim > /dev/null); then
  brew install neovim
fi

# NeoVim
if [ -d "$HOME/.config/nvim" ]; then
  	rm -rf ~/.config/nvim
fi

mkdir -p ~/.config/nvim
ln -s $(ls "`pwd`/home/.config/nvim/init.vim") ~/.config/nvim/init.vim

# Vim-Plug
if [ ! -f "$HOME/.local/share/nvim/site/autoload/plug.vim" ]; then
  curl -fLo ~/.local/share/nvim/site/autoload/plug.vim --create-dirs \
  	https://raw.githubusercontent.com/junegunn/vim-plug/master/plug.vim
fi

nvim silent! +PlugUpdate +qa >/dev/null 2>&1
