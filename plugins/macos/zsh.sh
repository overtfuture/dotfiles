#!/bin/bash

if !(brew ls --versions zsh > /dev/null); then
  brew install zsh
fi

if [ ! -d "$HOME/.oh-my-zsh" ]; then
  echo "no zsh folder, cloning oh-my-zsh"
  git clone https://github.com/robbyrussell/oh-my-zsh.git $HOME/.oh-my-zsh

  if [ ! -n "$ZSH" ]; then
    ZSH=$HOME/.oh-my-zsh
  fi

  # Remove Login Message
  if [ ! -f "$HOME/.hushlogin" ]; then
    touch $HOME/.hushlogin
  fi

  # Symlink .zshrc
  if [ -f "$HOME/.zshrc" ]; then
    rm -rf ~/.zshrc
  fi

  ln -s $(ls "$(pwd)/home/.zshrc") ~/.zshrc

  sudo chsh -s $(which zsh) $(whoami)
fi
