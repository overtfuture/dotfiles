#!/bin/bash

if !(brew ls --versions gnupg > /dev/null); then
  brew install gnupg
fi

if !(brew ls --versions pinentry-mac > /dev/null); then
  brew install pinentry-mac
fi

if [ ! -f $HOME/.gnupg/gpg-agent.conf ]
then

echo "Setting pin entry program"
mkdir -p $HOME/.gnupg
chown -R $(whoami) $HOME/.gnupg/
chmod 700 $HOME/.gnupg
chmod 600 $HOME/.gnupg/*

cat <<EOF > $HOME/.gnupg/gpg-agent.conf
# Connects gpg-agent to the OSX keychain via the brew-installed
# pinentry program from GPGtools
pinentry-program /usr/local/bin/pinentry-mac
EOF

cat >$(echo $TMPDIR)/gpg <<EOF
    %echo Generating a basic OpenPGP key
    Key-Type: default
    Key-Length: 4096
    Subkey-Type: default
    Subkey-Length: 4096
    Name-Real: $PROFILE_NAME
    Name-Comment: $PROFILE_COMMENT
    Name-Email: $PROFILE_EMAIL
    Expire-Date: 0
    Passphrase: $GPG_PASSPHRASE
    %echo done
EOF

gpg --batch --gen-key $(echo $TMPDIR)gpg

GPG_SIGNING_KEY=$(gpg --list-secret-keys | grep -E "[A-F0-9]{40}")

if [ -f $HOME/.gitconfig_private ]
then
  git config --file $HOME/.gitconfig_private --add user.signingkey $GPG_SIGNING_KEY
  git config --file $HOME/.gitconfig_private --add commit.gpgsign "true"
fi

killall gpg-agent
# Run gpg-agent in daemon mode
gpg-agent --daemon

echo "GPG successfully set up!"
echo "Run the following command to save your passphrase in macOS Keychain:"
echo 'echo "Save to keychain" | gpg --clearsign'
echo "Export your public key using the following command:"
echo 'gpg --export --armor $PROFILE_EMAIL'
fi
