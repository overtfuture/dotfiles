#!/bin/bash

if !(brew ls --versions git > /dev/null); then
  brew install git
fi

# Symlink .gitconfig
if [ -f "$HOME/.gitconfig" ]; then
  rm -rf ~/.gitconfig
fi
ln -s $(ls "`pwd`/home/.gitconfig") ~/.gitconfig

echo ".gitconfig is now linked, place user configurations in ~/.gitconfig_private"

if [ ! -f $HOME/.gitignore_global ]
then
    touch $HOME/.gitignore_global
fi

if [ ! -f $HOME/.gitconfig_private ]
then
  touch $HOME/.gitconfig_private

  git config --file $HOME/.gitconfig_private --add user.name "$PROFILE_NAME"
  git config --file $HOME/.gitconfig_private --add user.email "$PROFILE_EMAIL"
  git config --file $HOME/.gitconfig_private --add credential.helper osxkeychain
fi