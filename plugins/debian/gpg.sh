#!/bin/bash

sudo apt-get install -y --fix-missing git gnupg ca-certificates >/dev/null

echo "Setting pin entry program"

cat >./gpg <<EOF
    Key-Type: default
    Key-Length: 4096
    Subkey-Type: default
    Subkey-Length: 4096
    Name-Real: $PROFILE_NAME
    Name-Comment: $PROFILE_COMMENT
    Name-Email: $PROFILE_EMAIL
    Expire-Date: 0
    Passphrase: $GPG_PASSPHRASE
EOF

gpg --batch --gen-key ./gpg

GPG_SIGNING_KEY=$(gpg --list-secret-keys | grep -E "[A-F0-9]{40}")

if [ -f $HOME/.gitconfig_private ]
then
  git config --file $HOME/.gitconfig_private --add user.signingkey $GPG_SIGNING_KEY
  git config --file $HOME/.gitconfig_private --add commit.gpgsign "true"
fi

if [ ! -f $HOME/.gnupg/gpg.conf ]
then
  cat >$HOME/.gnupg/gpg.conf <<EOF
    pinentry-mode loopback
EOF
fi

if [ ! -f $HOME/.gnupg/gpg-agent.conf ]
then
  cat >$HOME/.gnupg/gpg-agent.conf <<EOF
    pinentry-program pinentry-curses
    default-cache-ttl-ssh 60480000
    max-cache-ttl-ssh 60480000
EOF
fi

gpg-connect-agent reloadagent /bye
rm -rf ./gpg

echo "GPG successfully set up!"
echo "Run the following command to save your passphrase:"
echo 'echo "Save to keychain" | gpg --clearsign'
echo "Export your public key using the following command:"
echo "gpg --export --armor $PROFILE_EMAIL"