#!/bin/bash
sudo apt-get install -y git >/dev/null

git clone --depth 1 https://github.com/junegunn/fzf.git $HOME/.fzf
$HOME/.fzf/install
