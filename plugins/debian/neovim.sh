#!/bin/bash
sudo apt-get install -y curl neovim >/dev/null

# NeoVim
if [ -d "$HOME/.config/nvim" ]; then
  	rm -rf ~/.config/nvim
fi

mkdir -p $HOME/.config/nvim
ln -s $(ls "`pwd`/home/.config/nvim/init.vim") $HOME/.config/nvim/init.vim

# Vim-Plug
if [ ! -f "$HOME/.local/share/nvim/site/autoload/plug.vim" ]; then
  curl -fLo $HOME/.local/share/nvim/site/autoload/plug.vim --create-dirs https://raw.githubusercontent.com/junegunn/vim-plug/master/plug.vim
fi

# nvim silent! +PlugUpdate +qa >/dev/null 2>&1
nvim silent! +'PlugInstall --sync' +qa

echo "Installed NeoVim"